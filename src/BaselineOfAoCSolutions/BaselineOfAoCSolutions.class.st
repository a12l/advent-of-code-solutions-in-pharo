Class {
	#name : #BaselineOfAoCSolutions,
	#superclass : #BaselineOf,
	#category : #BaselineOfAoCSolutions
}

{ #category : #accessing }
BaselineOfAoCSolutions class >> load2021Solutions [
	"Add the lepiter notebook with the AoC 2021 solutions."
	
	| currentProperties |
	currentProperties := LeDatabasesRegistry defaultLogicalDatabase properties.
	currentProperties
		addRegisteredDirectory: FileLocator imageDirectory
			/ 'pharo-local'
			/ 'iceberg'
			/ 'a12l'
			/ 'advent-of-code-solutions-in-pharo'
			/ 'lepiter'
			/ '2021'.
	
	LeDatabasesRegistry default defaultLogicalDatabase reload.
]

{ #category : #accessing }
BaselineOfAoCSolutions >> baseline: spec [
	<baseline>
	spec
		for: #common
		do: [
			spec
				package: 'AoCSolutions' ]
]
