Class {
	#name : #Day2PartAData,
	#superclass : #Object,
	#instVars : [
		'position'
	],
	#category : #'AoCSolutions-Data'
}

{ #category : #accessing }
Day2PartAData class >> atPosition: pos [
	^ self basicNew initializeAtPosition: pos.
]

{ #category : #initalize }
Day2PartAData >> initialize [
	position := (0, 0, 0).
]

{ #category : #initalize }
Day2PartAData >> initializeAtPosition: pos [
	position := pos.
]

{ #category : #accessing }
Day2PartAData >> position [
	^ position.
]
