Class {
	#name : #Day2,
	#superclass : #Object,
	#category : #'AoCSolutions-Solutions'
}

{ #category : #example }
Day2 >> import [
	<gtExample>
	
	| input |
	input := (AoCSolutions year: 2021 day: 2).
	input := CSVParser parseFileWithoutHeader: input.
	input := input collect: [ :e | e first splitOn: ' '. ].
	input := input collect: [ :e | OrderedCollection with: e first with: e second asInteger ].
	
	^ input.
]

{ #category : #example }
Day2 >> partASolution [
	<gtExample>
	
	| input submarine |
	input := self import.
	
	submarine := input
	inject: Day2PartAData new
	into: [ :s :e |
		| direction distance |
		direction := e first.
		distance := e second.
		Day2PartA moveSubmarine: s inDirection: direction distance: distance.
	].
	submarine := submarine position x * submarine position z.
	
	^ submarine.
]

{ #category : #example }
Day2 >> partBSolution [
	<gtExample>
	
	| input submarine |
	input := self import.
	
	submarine := input
	inject: Day2PartBData new
	into: [ :s :e |
		| direction distance |
		direction := e first.
		distance := e second.
		Day2PartB moveSubmarine: s inDirection: direction distance: distance.
	].
	submarine := submarine position x * submarine position z.
	
	^ submarine.
]
