Class {
	#name : #Day2PartBExample,
	#superclass : #Object,
	#category : #'AoCSolutions-Examples'
}

{ #category : #example }
Day2PartBExample >> isCreated [
	<gtExample>
	| submarine |
	submarine := Day2PartBData new.
	
	self assert: submarine position = (0, 0, 0).
	self assert: submarine aim = 0.
	
	^ submarine.
]

{ #category : #example }
Day2PartBExample >> moveDownEightSteps [
	<gtExample>
	| submarine |
	submarine := self moveUpThreeSteps.
	submarine := Day2PartB moveSubmarine: submarine inDirection: 'down' distance: 8.
	
	self assert: submarine position = (13, 0, 40).
	self assert: submarine aim = 10.
	
	^ submarine.
]

{ #category : #example }
Day2PartBExample >> moveDownFiveSteps [
	<gtExample>
	| submarine |
	submarine := self moveForwardFiveSteps.
	submarine := Day2PartB moveSubmarine: submarine inDirection: 'down' distance: 5.
	
	self assert: submarine position = (5, 0, 0).
	self assert: submarine aim = 5.
	
	^ submarine.
]

{ #category : #example }
Day2PartBExample >> moveForwardEightSteps [
	<gtExample>
	| submarine |
	submarine := self moveDownFiveSteps.
	submarine := Day2PartB moveSubmarine: submarine inDirection: 'forward' distance: 8.
	
	self assert: submarine position = (13, 0, 40).
	self assert: submarine aim = 5.
	
	^ submarine.
]

{ #category : #example }
Day2PartBExample >> moveForwardFiveSteps [
	<gtExample>
	| submarine |
	submarine := self isCreated.
	submarine := Day2PartB moveSubmarine: submarine inDirection: 'forward' distance: 5.
	
	self assert: submarine position = (5, 0, 0).
	self assert: submarine aim = 0.
	
	^ submarine.
]

{ #category : #example }
Day2PartBExample >> moveForwardTwoSteps [
	<gtExample>
	| submarine |
	submarine := self moveDownEightSteps.
	submarine := Day2PartB moveSubmarine: submarine inDirection: 'forward' distance: 2.
	
	self assert: submarine position = (15, 0, 60).
	self assert: submarine aim = 10.
	
	^ submarine.
]

{ #category : #example }
Day2PartBExample >> moveUpThreeSteps [
	<gtExample>
	| submarine |
	submarine := self moveForwardEightSteps.
	submarine := Day2PartB moveSubmarine: submarine inDirection: 'up' distance: 3.
	
	self assert: submarine position = (13, 0, 40).
	self assert: submarine aim = 2.
	
	^ submarine.
]
