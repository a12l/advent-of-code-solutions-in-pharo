Class {
	#name : #AoCSolutions,
	#superclass : #Object,
	#category : #'AoCSolutions-Code'
}

{ #category : #accessing }
AoCSolutions class >> year: year day: day [ 
	^ FileLocator imageDirectory
		/ 'pharo-local'
		/ 'iceberg'
		/ 'a12l'
		/ 'advent-of-code-solutions-in-pharo'
		/ 'res'
		/ 'input'
		/ year asString
		/ ('day', day asString, '.txt').
]
