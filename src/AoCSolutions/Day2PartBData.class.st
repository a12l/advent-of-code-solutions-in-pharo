Class {
	#name : #Day2PartBData,
	#superclass : #Object,
	#instVars : [
		'position',
		'aim'
	],
	#category : #'AoCSolutions-Data'
}

{ #category : #accessing }
Day2PartBData class >> atPosition: pos withAim: newAim [
	^ self basicNew initializeAtPosition: pos withAim: newAim.
]

{ #category : #accessing }
Day2PartBData >> aim [
	^ aim.
]

{ #category : #initalize }
Day2PartBData >> initialize [
	position := (0, 0, 0).
	aim := 0.
]

{ #category : #accessing }
Day2PartBData >> initializeAtPosition: pos withAim: newAim [
	position := pos.
	aim := newAim.
]

{ #category : #accessing }
Day2PartBData >> position [
	^ position.
]
