Class {
	#name : #Day1,
	#superclass : #Object,
	#category : #'AoCSolutions-Solutions'
}

{ #category : #example }
Day1 >> import [
	<gtExample>
	
	| input |
	input := (AoCSolutions year: 2021 day: 1).
	input := CSVParser parseFileWithoutHeader: input.
	input := input collect: [ :e | e asString asInteger ].
	
	^ input.
]

{ #category : #example }
Day1 >> partASolution [
	<gtExample>
	
	| input result |
	input := self import.
	
	result := 0.
	2
		to: input size
		do: [ :n |
			((input at: n) > (input at: n - 1))
				ifTrue: [ result := (result + 1) ]
		].

	^ result.
]

{ #category : #example }
Day1 >> partBGroupedDepths [
	<gtExample>
	
	| input threeMeasurements |
	input := self import.
	
	threeMeasurements := OrderedCollection new.
	1
		to: input size - 2
		do: [ :n |
			threeMeasurements add: (input at: n) + (input at: n + 1) + (input at: n + 2).
		].

	^ threeMeasurements.
]

{ #category : #example }
Day1 >> partBResult [
	<gtExample>
	
	| input result |
	input := self partBGroupedDepths.
	
	result := 0.
	2
		to: input size
		do: [ :n |
		((input at: n) > (input at: n - 1))
			ifTrue: [ result := (result + 1) ]
	].

	^ result.
]
