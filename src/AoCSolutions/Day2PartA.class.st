Class {
	#name : #Day2PartA,
	#superclass : #Object,
	#category : #'AoCSolutions-Code'
}

{ #category : #accessing }
Day2PartA class >> moveSubmarine: submarine inDirection: direction distance: distance [
	| x y z |
	x := submarine position x.
	y := submarine position y.
	z := submarine position z.
	
	^ direction = 'up'
		ifTrue: [
			(z > distance)
				ifTrue: [ Day2PartAData atPosition: (x, y, (z - distance)) ]
				ifFalse: [ Day2PartAData atPosition: (x, y, 0) ]
		]
		ifFalse: [
			direction = 'forward'
				ifTrue: [ Day2PartAData atPosition: ((x + distance), y, z) ]
				ifFalse: [
					direction = 'down'
						ifTrue: [ Day2PartAData atPosition: (x, y, (z + distance)) ]
				]
		]
]
