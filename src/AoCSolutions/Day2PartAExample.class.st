Class {
	#name : #Day2PartAExample,
	#superclass : #Object,
	#category : #'AoCSolutions-Examples'
}

{ #category : #example }
Day2PartAExample >> isCreated [
	"This is a new method"
	<gtExample>
	| submarine |
	submarine := Day2PartAData new.
	
	self assert: submarine position = (0, 0, 0).
	
	^ submarine.
]

{ #category : #example }
Day2PartAExample >> moveDownTenSteps [
	<gtExample>
	| submarine |
	submarine := (OrderedCollection with: (OrderedCollection with: 'down' with: 10))
		inject: self moveForwardOneStep
		into: [ :s :e |
			| direction distance |
			direction := e first.
			distance := e second.
			Day2PartA moveSubmarine: s inDirection: direction distance: distance.
		].
	
	self assert: submarine position = (1, 0, 10).
	
	^ submarine.
]

{ #category : #example }
Day2PartAExample >> moveForwardOneStep [
	"This is a new method"
	<gtExample>
	| submarine |
	submarine := self isCreated.
	submarine := Day2PartA moveSubmarine: submarine inDirection: 'forward' distance: 1.
	
	self assert: submarine position = (1, 0, 0).
	
	^ submarine
]

{ #category : #example }
Day2PartAExample >> moveToSurface [
	<gtExample>
	| submarine |
	submarine := (OrderedCollection with: (OrderedCollection with: 'up' with: 11))
		inject: self moveDownTenSteps
		into: [ :s :e |
			| direction distance |
			direction := e first.
			distance := e second.
			Day2PartA moveSubmarine: s inDirection: direction distance: distance.
		].
	
	self assert: submarine position = (1, 0, 0).
	
	^ submarine.
]

{ #category : #example }
Day2PartAExample >> moveUpFiveSteps [
	<gtExample>
	| submarine |
	submarine := (OrderedCollection with: (OrderedCollection with: 'up' with: 5))
		inject: self moveDownTenSteps
		into: [ :s :e |
			| direction distance |
			direction := e first.
			distance := e second.
			Day2PartA moveSubmarine: s inDirection: direction distance: distance.
		].
	
	self assert: submarine position = (1, 0, 5).
	
	^ submarine.
]
