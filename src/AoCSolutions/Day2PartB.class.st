Class {
	#name : #Day2PartB,
	#superclass : #Object,
	#category : #'AoCSolutions-Code'
}

{ #category : #accessing }
Day2PartB class >> moveSubmarine: submarine inDirection: direction distance: distance [
	| x y z currentAim |
	x := submarine position x.
	y := submarine position y.
	z := submarine position z.
	currentAim := submarine aim.
	
	^ direction = 'up'
		ifTrue: [
			(z > distance)
				ifTrue: [ Day2PartBData atPosition: (x, y, z) withAim: currentAim - distance ]
				ifFalse: [ Day2PartBData atPosition: (x, y, z) withAim: currentAim - z ]
		]
		ifFalse: [
			direction = 'forward'
				ifTrue: [ Day2PartBData atPosition: (x + distance, y, (z + (currentAim * distance))) withAim: currentAim ]
				ifFalse: [
					direction = 'down'
						ifTrue: [ Day2PartBData atPosition: (x, y, z) withAim: currentAim + distance ]
				]
		]
]
