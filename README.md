# Advent of Code Solutions in Pharo

## Importing the repository

The easiest way to import this repository is by copy and paste

```
Metacello new
	baseline: 'AoCSolutions';
	repository: 'gitlab://a12l/advent-of-code-solutions-in-pharo:trunk/src';
	load.
```
 into a Pharo snippet and run the code. This will download and import the repository into Glamorous Toolkit.
 
The notebooks isn't automatically imported when you import the repository. To import them, you need to run one or more of the snippets 

```
BaselineOfAoCSolutions load2021Solutions.
```

to have them available in the Lepiter notebook lists.